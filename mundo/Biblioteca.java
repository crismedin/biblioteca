package mundo;
import util.*;
import util_archivo.*;

public class Biblioteca
{
    private String nombre;
    private ListaCD<Facultad> facultades=new ListaCD();
    private ListaCD<Estudiante> estudiantes=new ListaCD();
    private ListaCD<Mensaje> mensajes=new ListaCD();
    private ColaP<Solicitud> solicitudes=new ColaP();

    /**
     * Constructor for objects of class Biblioteca
     */
    public Biblioteca()
    {
        
    }

    public Biblioteca(String nombre, String urlFac, String urlLibros, String urlEstd,String urlSol)
    {
        // initialise instance variables
        this.nombre=nombre;
        //Crear facultades:
        this.crearFacultad(urlFac);
        //Crear Libros
        this.crearLibros(urlLibros);
        //Crear Estudiantes
        this.crearEstudiantes(urlEstd);
        //Crear Solicitudes
        this.crearSolicitudes(urlSol);

    }

    private void crearSolicitudes(String urlSol)
    {
        ArchivoLeerURL file=new ArchivoLeerURL(urlSol);
        Object v[]=file.leerArchivo();
        for(int i=1; i<v.length;i++)
        {
            String fila=v[i].toString();
            String datos[]=fila.split(";");

            int codLibro=Integer.parseInt(datos[0]);
            int tipoServicio=Integer.parseInt(datos[1]);
            int codEst=Integer.parseInt(datos[2]);

            //Crea un estudiante temporal para realizar la busqueda sobre los estudiantes activos
            Estudiante estDesconocido=new Estudiante();
            estDesconocido.setCodigo(codEst);

            estDesconocido= this.getEstudiante(estDesconocido);
            if(estDesconocido==null){
                this.mensajes.insertarAlFinal(new Mensaje("El estudiante con codigo "+codEst +" no existe"));
            }else{
                Solicitud nueva= new Solicitud();
                nueva.setEst_desconocido(estDesconocido);
                nueva.setLibro_desconocido(new Libro(codLibro,""));
                nueva.setTipo_servicio(tipoServicio);
                this.solicitudes.enColar(nueva,(100-estDesconocido.getSemestre()));
            }

        }
    }

    private Estudiante getEstudiante(Estudiante x){

        for(Estudiante est: this.estudiantes){
            if(est.equals(x)){
                return est;
            }
        }
        return null;
    }

    public void procesarSolicitudes()
    {
        String men="";
        while(!this.solicitudes.esVacia()){
            Solicitud s = this.solicitudes.deColar();
            System.out.println(s.toString()); 
            boolean validarEst = validarEstudiante(s.getEst_desconocido());
            boolean validarCod = validarCodigo(s.getLibro_desconocido());
            if(validarEst && validarCod){
            
                switch(s.getTipo_servicio()){
                
                    case 1:{
                            if(s.getEst_desconocido().esValidoReservar())
                                   men = "Se reservo el libro con codigo "+s.getLibro_desconocido().getCodigo()+" al estudiante de codigo "+s.getEst_desconocido().getCodigo()+"";
                            else
                                   men = "No se reservó el libro con código "+s.getLibro_desconocido().getCodigo()+" POR QUE el estudiante con código: "+s.getEst_desconocido().getCodigo()+" excedió su cupo";
                    }
                    case 2:{
                            men = "Se entrego el libro con codigo "+s.getLibro_desconocido().getCodigo()+" al estudiante de codigo "+s.getEst_desconocido().getCodigo()+""; 
                    }
                }
            }
            if(!validarCod && (s.getTipo_servicio()==1))
                men = "No se reservó el libro con código "+s.getLibro_desconocido().getCodigo()+" no se encontraba en el inventario";
            this.mensajes.insertarAlFinal(new Mensaje(men));
        }
    }
    
    public boolean validarCodigo(Libro s){
        
        for(Facultad f : this.facultades){
            if(f.libroExiste(s)){
                return true;
            }
        }
        this.mensajes.insertarAlFinal(new Mensaje("El codigo del libro "+s.getCodigo()+" es invalido"));
        return false;
    }
    
    public boolean validarEstudiante(Estudiante estudiante){
        
        for(Estudiante e : this.estudiantes){
            if(e.equals(estudiante))
                return true;
        }
        
        this.mensajes.insertarAlFinal( new Mensaje("El codigo del estudainte "+estudiante.getCodigo()+" es invalido"));
        return false;
    }

    public String getListadoMensajes()
    {
        return (this.mensajes.toString());
    }

    private void crearEstudiantes(String urlEstd)
    {
        ArchivoLeerURL file=new ArchivoLeerURL(urlEstd);
        Object v[]=file.leerArchivo();
        for(int i=1; i<v.length;i++)
        {
            String fila=v[i].toString();
            String datos[]=fila.split(";");
            int cod=Integer.parseInt(datos[0]);
            int sem=Integer.parseInt(datos[3]);
            String nombre=datos[1];
            String email=datos[2];
            //Crear Estudiante:
            Estudiante nuevo=new Estudiante(cod, nombre, email, sem);
            //adicionar este estudiante a la lista de estudiantes:
            this.estudiantes.insertarAlFinal(nuevo);

        }

    }
    
    private void crearFacultad(String urlFac)
    {
        ArchivoLeerURL file=new ArchivoLeerURL(urlFac);
        Object v[]=file.leerArchivo();

        for(int i=1; i<v.length;i++)
        {
            String fila=v[i].toString();
            String datos[]=fila.split(";");
            int cod=Integer.parseInt(datos[0]);
            String nombre=datos[1];
            //Crear facultad:
            Facultad f=new Facultad(nombre,cod);
            this.facultades.insertarAlFinal(f);
        }

    }

    private void crearLibros(String urlLibros)
    {
        ArchivoLeerURL file=new ArchivoLeerURL(urlLibros);
        Object v[]=file.leerArchivo();

        for(int i=1; i<v.length;i++)
        {
            String fila=v[i].toString();
            String datos[]=fila.split(";");
            int cod=Integer.parseInt(datos[0]);
            String nombre=datos[1];
            //Crear facultad:
            Libro l=new Libro(cod,nombre);
            int primer=cod/100000;

            this.facultades.get(primer-1).addLibro(l);

        }
    }

    public String getListadoEstudiantes()
    {
        return this.estudiantes.toString();
    }

    public String getListadoFacultad()
    {

        return this.facultades.toString();

    }
    //Este metodo borra la cola de solicitudes
    public String getListaSolicitudes()
    {
        String aux="";
        while(!solicitudes.esVacia()){
          aux+=solicitudes.deColar().toString()+"\n";
          
        }
        return aux;
    }

    

}
