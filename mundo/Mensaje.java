package mundo;


/**
 * Write a description of class Mensaje here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Mensaje
{
    
    private String descripcion;
    
    public Mensaje()
    {
        // initialise instance variables
    
    }
    
    public Mensaje(String d)
    {
        this.descripcion=d;
    }

    
    public String toString()
    {
        return (this.descripcion+"\n");
    }

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie descripcion*/
    public String getDescripcion(){
        return this.descripcion;
    }//end method getDescripcion

    /**SET Method Propertie descripcion*/
    public void setDescripcion(String descripcion){
        this.descripcion = descripcion;
    }//end method setDescripcion

    //End GetterSetterExtension Source Code
//!
}
