package mundo;


/**
 * Write a description of class Estudiante here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Estudiante
{
    // instance variables - replace the example below with your own
    private int codigo;
    private String nombre;
    private String email;
    private int semestre;
    private int cantLibros=0;

    /**
     * Constructor for objects of class Estudiante
     */
    
        public Estudiante(int codigo, String nombre, String email, int semestre)
    {
        // initialise instance variables
        this.codigo=codigo;
        this.nombre=nombre;
        this.email=email;
        this.semestre=semestre;
    }
    
    
    
    public Estudiante()
    {
        // initialise instance variables
        
    }

    
    public boolean esValidoReservar()
    {
        return (this.cantLibros<2);
    }
    
    public boolean reservar()
    {
        if(this.esValidoReservar())
                {
                    this.cantLibros++;
                    return true;
                }
         return false;
    }
    
    
    public String toString()
    {
        return (this.codigo+"\t"+this.nombre+"\t"+this.email+"\t"+this.semestre);
        
    }
    
    
    public boolean equals(Object c)
    {   
        Estudiante x=(Estudiante)c;
        return x.getCodigo()==this.getCodigo();
    }

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie codigo*/
    public int getCodigo(){
        return this.codigo;
    }//end method getCodigo

    /**SET Method Propertie codigo*/
    public void setCodigo(int codigo){
        this.codigo = codigo;
    }//end method setCodigo

    /**GET Method Propertie nombre*/
    public String getNombre(){
        return this.nombre;
    }//end method getNombre

    /**SET Method Propertie nombre*/
    public void setNombre(String nombre){
        this.nombre = nombre;
    }//end method setNombre

    /**GET Method Propertie email*/
    public String getEmail(){
        return this.email;
    }//end method getEmail

    /**SET Method Propertie email*/
    public void setEmail(String email){
        this.email = email;
    }//end method setEmail

    /**GET Method Propertie semestre*/
    public int getSemestre(){
        return this.semestre;
    }//end method getSemestre

    /**SET Method Propertie semestre*/
    public void setSemestre(int semestre){
        this.semestre = semestre;
    }//end method setSemestre

    /**GET Method Propertie cantLibros*/
    public int getCantLibros(){
        return this.cantLibros;
    }//end method getCantLibros

    /**SET Method Propertie cantLibros*/
    public void setCantLibros(int cantLibros){
        this.cantLibros = cantLibros;
    }//end method setCantLibros

    //End GetterSetterExtension Source Code
//!
}
