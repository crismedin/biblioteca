package mundo;


/**
 * Write a description of class Solicitud here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Solicitud
{
    
    private Estudiante est_desconocido;
    private Libro  libro_desconocido;
    private int tipo_servicio;

    /**
     * Constructor for objects of class Solicitud
     */
    public Solicitud()
    {
        
       
    }
    
    

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie est_desconocido*/
    public Estudiante getEst_desconocido(){
        return this.est_desconocido;
    }//end method getEst_desconocido

    /**SET Method Propertie est_desconocido*/
    public void setEst_desconocido(Estudiante est_desconocido){
        this.est_desconocido = est_desconocido;
    }//end method setEst_desconocido

    /**GET Method Propertie libro_desconocido*/
    public Libro getLibro_desconocido(){
        return this.libro_desconocido;
    }//end method getLibro_desconocido

    /**SET Method Propertie libro_desconocido*/
    public void setLibro_desconocido(Libro libro_desconocido){
        this.libro_desconocido = libro_desconocido;
    }//end method setLibro_desconocido

    /**GET Method Propertie tipo_servicio*/
    public int getTipo_servicio(){
        return this.tipo_servicio;
    }//end method getTipo_servicio

    /**SET Method Propertie tipo_servicio*/
    public void setTipo_servicio(int tipo_servicio){
        this.tipo_servicio = tipo_servicio;
    }//end method setTipo_servicio

    public String toString()
    {
        return (this.tipo_servicio+","+this.est_desconocido.toString()+","+this.libro_desconocido.toString());
    }
    
    //End GetterSetterExtension Source Code
//!
}
