package mundo;
import util.*;

/**
 * Write a description of class Facultad here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Facultad
{
    // instance variables - replace the example below with your own
   
    private String nombre;
    private int codigo;
    Pila<Libro> libros=new Pila();
    
    /**
     * Constructor for objects of class Facultad
     */
    public Facultad()
    {
        // initialise instance variables
       
    }
    
    public Facultad(String nombre, int cod)
    {
        this.nombre=nombre;
        this.codigo=cod;
        
    }
    
    public boolean libroExiste(Libro l1){
        Pila<Libro> aux = new Pila();
        Libro l = null;
        while(!this.libros.esVacia()){
            l = this.libros.desapilar();
            aux.apilar(l);
            if(l.equals(l1)){break;}
        }
        
        while(!aux.esVacia()){
            this.libros.apilar(aux.desapilar());
        }
        if(l==null)
            return false;
        return l.equals(l1);
    }
  
    public String getNombre(){
        return this.nombre;
    }//end method getNombre

    /**SET Method Propertie nombre*/
    public void setNombre(String nombre){
        this.nombre = nombre;
    }//end method setNombre

    /**GET Method Propertie codigo*/
    public int getCodigo(){
        return this.codigo;
    }//end method getCodigo

    /**SET Method Propertie codigo*/
    public void setCodigo(int codigo){
        this.codigo = codigo;
    }//end method setCodigo

    //End GetterSetterExtension Source Code
//!

    public String toString()
    {
        String m=this.getListadoLibros();
        return ("Codigo:"+this.getCodigo()+"\t Nombre Fac:"+this.getNombre()+"\n"+m);
    }

    
    public void addLibro(Libro l)
    {
        this.getLibros().apilar(l);
    }


    //Start GetterSetterExtension Source Code
    /**GET Method Propertie libros*/
    public util.Pila<mundo.Libro> getLibros(){
        return this.libros;
    }//end method getLibros

    
    public String getListadoLibros()
    {
        String m="";
        while (!this.libros.esVacia())
        {
            //Esto es un libro dentro de la pila: this.libros.desapilar().
            m+=this.libros.desapilar().toString()+"\n";
            
        }
    return m;
    }
    
    //End GetterSetterExtension Source Code
//!
}
